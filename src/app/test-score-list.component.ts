import { Component, OnInit } from '@angular/core';
import { TestScore } from './test-score';
import { TestScoreService } from './test-score.service';

@Component({
	selector: 'test-score-list',
	templateUrl: './test-score-list.component.html'
})
export class TestScoreListComponent implements OnInit {
	testScores: Array<TestScore>;

	constructor(private testScoreService: TestScoreService) {}

	ngOnInit(): void {
		this.testScoreService.testScores.subscribe(testScores => {
			console.log('it changed');
			console.log(testScores)
			this.testScores = testScores;
		});
	}

	addTestScore(): void {
		this.testScores.unshift(new TestScore());
	}

	getMin(): number {
		let scores = this.getScores();
		return Math.min(...scores);
	}

	getMax(): number {
		let scores = this.getScores();
		return Math.max(...scores);
	}

	getAvg(): number {
		let scores = this.getScores();

		let total = 0;
		for (let score of scores) {
			total += score;
		}

		return total/scores.length;
	}

	private getScores(): Array<number> {
		return this.testScores
			.map(testScore => testScore.score)
			.filter(score => !!score);
	}
}
