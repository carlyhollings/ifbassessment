import { Injectable } from '@angular/core';
import { TestScore } from './test-score';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable()
export class TestScoreService {
	constructor() {
		this.refreshTestScores();
	}

	testScores: BehaviorSubject<Array<TestScore>> = new BehaviorSubject([]);

	refreshTestScores(): void {
		this.testScores.next(this.getStoredTestScores());
	}

	saveTestScore(testScore: TestScore): Observable<TestScore> {
		if (testScore.id) {
			return this.updateTestScore(testScore);
		} else {
			return this.createTestScore(testScore);
		}
	}

	createTestScore(testScore: TestScore): Observable<TestScore> {
		testScore.id = this.nextId();
		let testScores = this.getStoredTestScores();
		testScores.push(testScore);
		this.storeTestScores(testScores);

		return Observable.of(testScore);
	}

	updateTestScore(testScore: TestScore): Observable<TestScore> {
		let testScores = this.testScores.value;
		let testScoreIndex = this.getTestScoreIndex(testScore.id, testScores);

		if (testScoreIndex == -1) {
			return this.createTestScore(testScore);
		}

		testScores[testScoreIndex] = testScore;
		this.storeTestScores(testScores);

		return Observable.of(testScore);
	}

	deleteTestScore(testScore: TestScore): Observable<any> {
		let testScores = this.testScores.value;
		let testScoreIndex = this.getTestScoreIndex(testScore.id, testScores);

		if (testScoreIndex == -1) {
			return Observable.of(true); //if the record was never saved, we don't need to do anything to delete it
		}

		testScores.splice(testScoreIndex, 1);
		this.storeTestScores(testScores);

		return Observable.of(true);
	}

	private getTestScoreIndex(id: number, testScores: Array<TestScore>) {
		return testScores.findIndex(testScore => {
			return testScore.id == id;
		});
	}

	private getStoredTestScores(): Array<TestScore> {
		let storedData = localStorage.getItem('testScores');
		if (!storedData) {
			return [];
		}
		return JSON.parse(storedData);
	}

	private storeTestScores(testScores: Array<TestScore>): void {
		localStorage.setItem('testScores', JSON.stringify(testScores));
		this.testScores.next(testScores);
	}

	private nextId(): number {
		let lastVal = JSON.parse(localStorage.getItem('testScoreIdSeq'));
		let nextVal = lastVal + 1;
		localStorage.setItem('testScoreIdSeq', JSON.stringify(nextVal));
		return nextVal;
	}
}
