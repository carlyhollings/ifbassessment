import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { TestScoreListComponent } from './test-score-list.component';
import { TestScoreDetailComponent } from './test-score-detail.component';
import { TestScoreService } from './test-score.service'

@NgModule({
	imports: [
		BrowserModule, 
		FormsModule
	],
	declarations: [
		AppComponent,
		TestScoreListComponent,
		TestScoreDetailComponent
	],
	bootstrap: [AppComponent],
	providers: [TestScoreService]
})
export class AppModule { }
