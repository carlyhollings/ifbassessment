import { Student } from './student';

export class TestScore {
	id: number;
	student: Student = new Student();
	score: number;
}
