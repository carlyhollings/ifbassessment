import { Component, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { TestScore } from './test-score';
import { TestScoreService } from './test-score.service';

@Component({
	selector: 'test-score-detail',
	templateUrl: './test-score-detail.component.html'
})
export class TestScoreDetailComponent {
	@Input() testScore: TestScore;

	constructor(private testScoreService: TestScoreService) {}

	saveTestScore(form: NgForm) {
		this.testScoreService.saveTestScore(this.testScore).subscribe(testScore => {
			form.resetForm(testScore);
		}, error => {
			alert('Could not save test score.');
		});
	}

	deleteTestScore() {
		this.testScoreService.deleteTestScore(this.testScore).subscribe(data => {
			//don't need to do anything on a successful delete
		}, error => {
			alert('Could not delete test score.');
		});
	}
}
